<?php

$form = new contactform(array(
  'to'       => 'Jessica Paoli <jes@sica.me>',
  'from'     => 'jessicapaoli.com <hello@jessicapaoli.com>',
  'subject'  => 'When it comes I wanna mail -- mail!',
  'goto'     => $page->url() . '/status:thank-you'
));

?>
<section id="contactform">

  <?php if(param('status') == 'thank-you'): ?>

  <h1>Thank you very much for your request</h1>
  <p class="contactform-text">We will get in contact as soon as possible</p>

  <?php else: ?>


  <form action="#contactform" method="post" class="contact-form">
      <?php if($form->isError('send')): ?>
        <span class="error-alert">Something went wrong but it&rsquo;s not your fault. Please try again!</span>
      <?php elseif($form->isError()): ?>
        <span class="error-alert">Sorry, something went wrong (╯︵╰,) Please try again!</span>
      <?php endif ?>

      <div class="form-field--name form-field<?php if($form->isError('name')) echo ' error' ?>">
        <label class="form-label" for="contactform-name">Name <?php if($form->isRequired('name')) echo '<span class="form-required">*</span>' ?> <?php if($form->isError('name')): ?><span class="error-context">You actin’ kinda shady&hellip;</small><?php endif ?></label>
        <input class="form-field--input" type="text" id="contactform-name" name="name" value="<?php echo $form->htmlValue('name') ?>" />
      </div>

      <div class="form-field--email form-field<?php if($form->isError('email')) echo ' error' ?>">
        <label class="form-label" for="contactform-email">Email <?php if($form->isRequired('email')) echo '<span class="form-required">*</span>' ?> <?php if($form->isError('email')): ?><span class="error-context">I need a way to reply!</small><?php endif ?></label>
        <input class="form-field--input" type="text" id="contactform-email" name="email" value="<?php echo $form->htmlValue('email') ?>" />
      </div>

      <div class="textarea form-field<?php if($form->isError('text')) echo ' error' ?>">
        <label class="form-label" for="contactform-text">Tell me about yourself, your project, or just say hi <?php if($form->isRequired('text')) echo '<span class="form-required">*</span>' ?> <?php if($form->isError('text')): ?><span class="error-context">Nothing to say?</small><?php endif ?></label>
        <textarea class="form-field--textarea" name="text" id="contactform-text"><?php echo $form->htmlValue('text') ?></textarea>
      </div>

      <button class="contactform-button" type="submit" name="submit">Send Message <svg version="1.1" id="arrow-right" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 42.305 35.032" enable-background="new 0 0 42.305 35.032" xml:space="preserve">
<path d="M19.219,1.438c0.429-0.429,0.967-0.781,1.611-1.055c0.645-0.273,1.269-0.4,1.875-0.381c0.604,0.02,1.23,0.146,1.875,0.381
  s1.181,0.606,1.611,1.113l16.113,15.996L26.191,33.605c-0.196,0.196-0.439,0.391-0.732,0.586c-0.293,0.196-0.586,0.352-0.879,0.469
  s-0.606,0.215-0.938,0.293c-0.333,0.079-0.636,0.098-0.908,0.059c-0.352,0.039-0.674,0.021-0.967-0.059
  c-0.293-0.078-0.606-0.176-0.938-0.293c-0.333-0.117-0.625-0.272-0.879-0.469c-0.254-0.195-0.518-0.39-0.791-0.586
  c-0.469-0.547-0.831-1.113-1.084-1.699c-0.254-0.586-0.372-1.21-0.352-1.875c0.019-0.664,0.136-1.298,0.352-1.904
  c0.214-0.605,0.595-1.162,1.143-1.67l3.867-3.926L5.039,22.473c-0.703,0.039-1.348-0.078-1.934-0.352
  c-0.586-0.272-1.124-0.624-1.611-1.055c-0.489-0.43-0.85-0.957-1.084-1.582S0.039,18.215,0,17.551
  c0.039-0.742,0.176-1.406,0.41-1.992s0.595-1.113,1.084-1.582c0.488-0.469,1.025-0.82,1.611-1.055s1.23-0.371,1.934-0.41
  l18.047,0.059l-3.867-4.043c-0.547-0.429-0.928-0.967-1.143-1.611c-0.215-0.645-0.333-1.279-0.352-1.904
  c-0.02-0.625,0.097-1.25,0.352-1.875c0.253-0.625,0.615-1.172,1.084-1.641L19.219,1.438z"/>
</svg>
</button>

  </form>

  <?php endif ?>

</section>