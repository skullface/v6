<!DOCTYPE html>
<html lang="en">
<head>

    <title><?php echo html($site->title()) ?>s <?php echo html($page->title()) ?></title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width">
    <meta name="description" content="<?php echo html($site->description()) ?>" />
    <meta name="keywords" content="<?php echo html($site->keywords()) ?>" />
    <meta name="robots" content="index, follow" />

    <?php echo css('assets/css/style.css') ?>
    <script type="text/javascript" src="//use.typekit.net/fgx5ibe.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>
    <link rel="stylesheet" type="text/css" href="//cloud.typography.com/653168/751482/css/fonts.css" />

    <link rel="apple-touch-icon" href="<?= url('assets/images/favico-152.png'); ?>">
    <link rel="icon" href="<?= url('assets/images/favico.png'); ?>">

</head>

<body class="site">

<header class="site-header">
    <a href="http://localhost/v6">
        <svg class="site-logo" version="1.1"
             xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
             x="0px" y="0px" viewBox="0 0 35.144 39.635" enable-background="new 0 0 35.144 39.635"
             xml:space="preserve">
            <g>
                <path d="M15.023,10.561C6.726,10.561,0,17.171,0,25.325c0,4.053,1.663,7.724,4.354,10.392h2.323c0,0,0.742,2.518,2.522,2.74
                    c0,0,0.519,0.371,1.781-1.261c0,0,1.855,5.49,4.303,0c0,0,2.152,5.416,4.006,0c0,0,0.766,1.115,1.876,1.315
                    c1.687-0.302,2.427-2.72,2.427-2.72h2.026c2.735-2.674,4.428-6.375,4.428-10.466C30.047,17.171,23.321,10.561,15.023,10.561z
                     M6.325,31.411c-0.257,0.697-0.624,0.856-1.031,0.856c-0.389,0-0.742-0.122-0.997-0.767c-0.255-0.645-0.413-1.812-0.413-2.796
                    c0-0.678,0.075-1.866,0.205-2.406c0.247-1.026,0.694-1.157,1.204-1.157c0.51,0,0.957,0.132,1.205,1.157
                    c0.131,0.545,0.205,1.727,0.205,2.405C6.704,29.643,6.56,30.775,6.325,31.411z M17.93,30.437c-1.242,1.5-2.35-0.523-2.35-0.523
                    s-1.047,2.371-2.35,0.523c-1.282-1.818,2.374-4.415,2.374-4.415S19.354,28.716,17.93,30.437z M26.087,26.298
                    c0.131,0.545,0.205,1.727,0.205,2.405c0,0.94-0.144,2.071-0.379,2.708c-0.257,0.697-0.624,0.856-1.031,0.856
                    c-0.389,0-0.742-0.122-0.997-0.767c-0.255-0.645-0.413-1.812-0.413-2.796c0-0.678,0.075-1.866,0.205-2.406
                    c0.247-1.026,0.694-1.157,1.204-1.157C25.393,25.141,25.84,25.272,26.087,26.298z"/>
                <path d="M29.706,16.526l5.438-8.26l-5.128,1.971c0.079,0.332,0.031,0.694-0.169,1.004c-0.388,0.602-1.19,0.775-1.793,0.387
                    c-0.601-0.388-0.774-1.191-0.386-1.792c0.14-0.216,0.334-0.375,0.552-0.475l0.698-5.11l-4.334,2.554
                    c0.053,0.309-0.001,0.638-0.184,0.923c-0.388,0.602-1.191,0.775-1.792,0.387c-0.602-0.388-0.775-1.191-0.387-1.792
                    c0.161-0.25,0.396-0.421,0.656-0.514L21.978,0l-4.835,8.711c0,0,4.198,0.663,7.518,2.622
                    C27.514,13.015,29.706,16.526,29.706,16.526z"/>
            </g>
        </svg>

        Info
    </a>

    <span class="slash">/</span>

    <a href="http://localhost/v6/work">
        Work
    </a>
</header>

<main class="site-main" role="main">
  <section class="site-content">
    <h1><?php echo html($page->title()) ?></h1>