<?php snippet('header') ?>


<nav class="project-navigation">
  <li>
  <?php if($page->hasPrevVisible()): ?>
  <a href="<?php echo $page->prevVisible()->url() ?>" class="project-navigation--prev">
    <svg version="1.1"
       xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
       x="0px" y="0px" width="40.433px" height="80.797px" viewBox="0 0 40.433 80.797" enable-background="new 0 0 40.433 80.797"
       xml:space="preserve">
    <polygon points="40.433,0 40.433,15.313 15.314,40.382 40.433,65.407 40.433,80.797 0,40.384 "/>
    </svg>
  </a>
  <?php else : ?>
  <a class="project-navigation--prev project-navigation--inactive">
    <svg version="1.1"
       xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
       x="0px" y="0px" width="40.433px" height="80.797px" viewBox="0 0 40.433 80.797" enable-background="new 0 0 40.433 80.797"
       xml:space="preserve">
    <polygon points="40.433,0 40.433,15.313 15.314,40.382 40.433,65.407 40.433,80.797 0,40.384 "/>
    </svg>
  </a>
  <?php endif ?>
  </li>

  <li>
  <a href="http://localhost/v6/work" class="project-navigation--grid">
    <svg version="1.1"
       xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
       x="0px" y="0px" width="50.673px" height="50.28px" viewBox="0 0 50.673 50.28" enable-background="new 0 0 50.673 50.28"
       xml:space="preserve">
    <defs>
    </defs>
    <g>
      <g>
        <path d="M31.332,11.604H19.728V0h11.604V11.604z M31.332,30.945H19.728V19.341h11.604V30.945z M50.673,11.604
          H39.069V0h11.604V11.604z M50.673,30.945H39.069V19.341h11.604V30.945z"/>
      </g>
      <g>
        <path d="M12.066,11.604H0.462V0h11.604V11.604z M12.066,30.945H0.462V19.341h11.604V30.945z"/>
      </g>
      <g>
        <path d="M50.211,50.28H38.606V38.675h11.604V50.28z"/>
      </g>
      <g>
        <path d="M11.604,50.28H0V38.675h11.604V50.28z M30.945,50.28H19.341V38.675h11.604V50.28z"/>
      </g>
    </g>
    </svg>
  </a>
  </li>

  <li>
  <?php if($page->hasNextVisible()): ?>
  <a href="<?php echo $page->nextVisible()->url() ?>" class="project-navigation--next">
    <svg version="1.1"
       xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
       x="0px" y="0px" width="40.433px" height="80.797px" viewBox="0 0 40.433 80.797" enable-background="new 0 0 40.433 80.797"
       xml:space="preserve">
    <polygon points="0,0 0,15.313 25.119,40.382 0,65.394 0,80.797 40.433,40.384 "/>
    </svg>
  </a>
  <?php else : ?>
  <a class="project-navigation--next project-navigation--inactive">
    <svg version="1.1"
       xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:a="http://ns.adobe.com/AdobeSVGViewerExtensions/3.0/"
       x="0px" y="0px" width="40.433px" height="80.797px" viewBox="0 0 40.433 80.797" enable-background="new 0 0 40.433 80.797"
       xml:space="preserve">
    <polygon points="0,0 0,15.313 25.119,40.382 0,65.394 0,80.797 40.433,40.384 "/>
    </svg>
  </a>
  <?php endif ?>
  </li>
</nav>

  <article>

    <div class="project-information">
      <?php echo kirbytext($page->text()) ?>
    </div>


    <div class="project-visuals">
      <ul class="gallery">
      <?php if($page->hasVideos()): ?>
        <li>
          <?php
            $videos = array(
              $page->videos()->find('00-video.mp4')
            );

            snippet('video', array(
              'videos' => $videos,
              'thumb'  => $page->images()->find('00-video.jpg'),
              'width'  => 900,
              'height' => 675
            ));
          ?>
        </li>
      <?php endif ?>
      <?php if($page->hasImages()): ?>
        <?php foreach($page->images()->not('thumb.jpg')->not('thumb.gif')->not('video.jpg') as $image): ?>
        <li>
          <img src="<?php echo $image->url() ?>" alt="<?php echo $image->name() ?>" />
          <p><?php echo $image->caption() ?></p>
        </li>
        <?php endforeach ?>
      <?php endif ?>
      </ul>
    </div>

    <dl class="project-details">
      <?php if(!$page->direction()) { ?>
      <div class="tags--large">
        <dt>Tags</dt>
        <dd>
          <?php echo $page->tags() ?>
        </dd>
      </div>
      <?php
        }
        else {
      ?>
      <div class="tags--small">
        <dt>Tags</dt>
        <dd>
          <?php echo $page->tags() ?>
        </dd>
      </div>

      <div class="team">
        <dt>Team</dt>
        <dd>
          <?php if($page->agency() != "") : ?>
            Created at <?php echo html($page->agency()) ?>.<br>
          <?php endif; ?><?php echo $page->direction() ?>
        </dd>
      </div>
      <?php } ?>

      <div class="date">
        <dt>Date</dt>
        <dd>
          <?php echo $page->completion() ?>
        </dd>
      </div>

    </dl>

  </article>

<?php snippet('footer') ?>