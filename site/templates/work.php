<?php snippet('header') ?>

<ul class="project-grid js-masonry">
	<?php foreach($page->children() as $project): ?>
	<li>
		<a href="<?php echo $project->url() ?>">
			<img src="<?php echo $project->images()->last()->url() ?>">
			<span class="project-grid-label"><?php echo html($project->title()) ?></span>
    	</a>
  	</li>
  <?php endforeach ?>
</ul>

<?php snippet('footer') ?>